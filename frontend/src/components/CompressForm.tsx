import React, { useState, useRef, FormEvent } from 'react';
import useAsyncHook from '../hooks/compress-string';

const CompressString = () => {
    const [text, setText] = useState('');
    const textInputRef = useRef<HTMLInputElement>(null);
    const [compressedString, error] = useAsyncHook(text);
    const handleCompressString = (e: FormEvent) => {
        e.preventDefault();
        setText(textInputRef.current?.value as string);
    };

    return (
        <>
            <form className="compress-form" onSubmit={handleCompressString}>
                <input
                    className="compress-form__input"
                    type="text"
                    name="compress"
                    ref={textInputRef}
                    data-testid="inputValue"
                    required
                />
                <button
                    data-testid="submitButton"
                    className="compress-form__button"
                >
                    Compress now!
                </button>
            </form>
            {!error && (
                <h2 className="compress-form__text">
                    Result: {compressedString}
                </h2>
            )}
            {error && (
                <p className="compress-form__error">
                    Internal Server Error. Please make sure the Web Service is
                    running!
                </p>
            )}
        </>
    );
};

export default CompressString;
