// E2E Test
// uncomment only when you have backend (port: 8080) and frontend (port: 3000) running locally.
import puppeteer from 'puppeteer';

test('validate ui', async () => {
    const browser = await puppeteer.launch({
        headless: false,
        slowMo: 35,
    });
    const page = await browser.newPage();
    const app = 'http://localhost:3000/';
    await page.goto(app);

    await page.click('input');
    await page.type('input', 'abzuaaissna');
    await page.click('button');
    page.$eval('h2', e => e.innerHTML).then(result => {
        expect(result).toBe('Result: a4binssuz');
    });
    const input = await page.$eval('input', (e: any) => e.value);
    expect(input).toBe('abzuaaissna');
    await browser.close();
}, 16000);
