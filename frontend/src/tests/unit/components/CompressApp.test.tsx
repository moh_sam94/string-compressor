import React from 'react';
import { mount } from 'enzyme';
import CompressApp from '../../../components/CompressApp';

test('should render CompressApp correctly', () => {
    const wrapper = mount(<CompressApp />);
    expect(wrapper).toMatchSnapshot();
});
