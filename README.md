# Overview
- This is a spring boot application, implemented as a coding challenge.
- You can send a GET request to the endpoint below. Enter a string and it will be compressed if more than 2 of the same letter exist.
- Example: string entered: abzuaaissna	result: a4binssuz

## Backend

### Requirements:
- Maven
- Java 8

### Install
maven clean install

### Start
- from Intellij run WebserviceApplication
- from command line: ``` mvn spring-boot:run ```

### Endpoint
- http://localhost:8080/group/{string}

## Frontend

-   Navigate to frontend directory to run npm commands

### Requirements

-   Node

### Dependencies

-   This app depends on the Spring Boot Backend Service

#### Install

`npm install`

### Start Development Server

`npm run dev`

### Start Production Server

`npm run start`

### Test

`npm run test`
`npm run test-watch`

### Build

`npm run build`

Open [http://localhost:3000](http://localhost:3000) to view development server in the browser.
Open [http://localhost:5000](http://localhost:5000) to view production server in the browser.
