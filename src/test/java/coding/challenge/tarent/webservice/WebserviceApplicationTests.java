package coding.challenge.tarent.webservice;

import coding.challenge.tarent.webservice.controller.StringController;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import static org.assertj.core.api.Assertions.assertThat;

@SpringBootTest
class WebserviceApplicationTests {

	@Autowired
	StringController stringController;

	@Test
	public void contexLoads() {
		assertThat(stringController).isNotNull();
	}

}
