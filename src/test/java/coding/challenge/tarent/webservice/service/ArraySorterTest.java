package coding.challenge.tarent.webservice.service;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import static org.junit.jupiter.api.Assertions.assertArrayEquals;

@SpringBootTest
class ArraySorterTest {

    @Autowired
    ArraySorter arraySorter;

    @Test
    void sortMultipleNumbersArray() {

        int[] unsorted =  {5,1,4,3,2};
        int[] expected = {1,2,3,4,5};
        assertArrayEquals(expected, arraySorter.sort(unsorted));
    }

    @Test
    void sortOneNumberArray() {

        int[] unsorted =  {5};
        int[] expected = {5};
        assertArrayEquals(expected, arraySorter.sort(unsorted));
    }
}