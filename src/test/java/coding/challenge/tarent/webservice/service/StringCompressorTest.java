package coding.challenge.tarent.webservice.service;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import static org.junit.jupiter.api.Assertions.assertEquals;

@SpringBootTest
class StringCompressorTest {

    @Autowired
    StringCompressor stringCompressor;

    @Test
    public void testCompressSimple() {
        String uncompressed = "aaa";
        String compressed =
                stringCompressor.compress(uncompressed);
        assertEquals("a3", compressed);
    }
    @Test
    public void testCompressMoreComplex() {
        String uncompressed = "aabbbbcddddddd";
        String compressed =
                stringCompressor.compress(uncompressed);
        assertEquals("aab4cd7", compressed);
    }
    @Test
    public void testCompressWithNonOptimizedResult() {
        String uncompressed = "hello";
        String compressed =
                stringCompressor.compress(uncompressed);
        assertEquals(uncompressed, compressed);
    }
}