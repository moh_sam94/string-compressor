package coding.challenge.tarent.webservice.controller;

import coding.challenge.tarent.webservice.service.CharGrouper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@CrossOrigin("*")
public class StringController {

    @Autowired
    CharGrouper charGrouper;

    @ResponseBody
    @GetMapping("/group/{string}")
    public String group(@PathVariable("string") String str) {

        return charGrouper.group(str);
    }
}