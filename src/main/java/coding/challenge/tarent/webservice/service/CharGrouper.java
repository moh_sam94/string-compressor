package coding.challenge.tarent.webservice.service;

import org.springframework.beans.factory.annotation.Autowired;

public class CharGrouper {

    @Autowired
    ArraySorter arraySorter;

    @Autowired
    StringCompressor stringCompressor;

    public String group(String str) {

        int[] intArray = stringToIntArray(str);

        int [] sortedIntArray = arraySorter.sort(intArray);

        char [] charSortedArray = intArrayToCharArray(sortedIntArray);

        String string = new String(charSortedArray);

        return stringCompressor.compress(string);
    }

    public int[] stringToIntArray(String str) {
        char[] charArray = str.toCharArray();
        int[] intArray = new int[charArray.length];
        for(int i = 0; i < charArray.length; ++i) {
            intArray[i] = charArray[i];
        }
        return intArray;
    }

    public char[] intArrayToCharArray(int[] intArray) {
        char[] charArray = new char[intArray.length];
        for(int i = 0; i < intArray.length; ++i) {
            charArray[i] = (char) intArray[i];
        }
        return charArray;
    }

}